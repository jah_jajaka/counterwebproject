package com.qaagility.controller;

public class Count {

    public int divide (int first, int second) {
        if (first == 0) {
            return Integer.MAX_VALUE;
        }
        else {
            return first / second;
        }
    }

}
